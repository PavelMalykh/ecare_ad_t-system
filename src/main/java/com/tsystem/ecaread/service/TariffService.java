package com.tsystem.ecaread.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsystem.ecaread.entity.Tariff;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TariffService {

    private static final String url = "http://localhost:8082/eecare_war/advertisement";
    private static final OkHttpClient httpClient = new OkHttpClient();

    public ArrayList<Tariff> getTariffs() throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }

            Type tariffType = new TypeToken<List<Tariff>>() {
            }.getType();

            String responseString = null;
            try {
                responseString = response.body().string();
            } catch (IOException e) {
                System.out.println("Can't parse response" + e.getMessage());
            }

            ArrayList<Tariff> tariffs = new Gson().fromJson(responseString, tariffType);

            return tariffs;
        }
    }

}