package com.tsystem.ecaread.controller;

import com.tsystem.ecaread.entity.Tariff;
import com.tsystem.ecaread.service.TariffService;

import javax.faces.bean.ManagedBean;
import java.io.IOException;
import java.util.ArrayList;

@ManagedBean(name = "tariffController", eager = true)
public class TariffController {

    TariffService tariffService = new TariffService();

    public ArrayList<Tariff> getTariffs() {
        ArrayList<Tariff> tariffList;
        try {
            tariffList = tariffService.getTariffs();
            return tariffList;
        } catch (IOException e) {
            throw new AssertionError(e.getMessage());
        }
    }
}