package com.tsystem.ecaread.entity;

import java.math.BigDecimal;
import java.util.Set;

public class Tariff {

    private String name;
    private BigDecimal price;
    private Set<Option> optionSet;

    public Tariff() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Set<Option> getOptionSet() {
        return optionSet;
    }

    public void setOptionSet(Set<Option> optionSet) {
        this.optionSet = optionSet;
    }
}