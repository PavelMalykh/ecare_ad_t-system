package com.tsystem.ecaread.entity;


import com.tsystem.ecaread.controller.TariffController;

import javax.faces.bean.ManagedBean;
import java.io.IOException;
import java.util.List;

@ManagedBean(name = "tariffBean", eager = true)
public class TariffBean {

    private List<Tariff> tariffList;

    public List<Tariff> getTariffList() {
        return new TariffController().getTariffs();
    }
}